module gitlab.cern.ch/thartlan/pull-tracker

go 1.15

require (
	github.com/gophercloud/gophercloud v0.13.0
	github.com/influxdata/influxdb-client-go/v2 v2.1.0
	github.com/pkg/errors v0.9.1
	gopkg.in/gcfg.v1 v1.2.3
	gopkg.in/warnings.v0 v0.1.2 // indirect
	k8s.io/api v0.18.0
	k8s.io/apimachinery v0.18.0
	k8s.io/client-go v0.18.0
	k8s.io/klog/v2 v2.3.0
)
