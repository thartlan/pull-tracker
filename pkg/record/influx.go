package record

import (
	"context"
	"strings"
	"time"

	"gitlab.cern.ch/thartlan/pull-tracker/pkg/collect"

	"github.com/influxdata/influxdb-client-go/v2"
	"github.com/pkg/errors"

	"k8s.io/klog/v2"
)

func PushToInflux(region, influxURL string, pulls []*collect.Pull) error {
	client := influxdb2.NewClient(influxURL, "")

	writeAPI := client.WriteAPIBlocking("org", "results")

	t := time.Now()

	for _, pull := range pulls {
		locationParts := strings.Split(pull.ImageLocation, ":")
		if len(locationParts) != 2 {
			klog.Errorf("Image %s did not split nicely", pull.ImageLocation)
			continue
		}
		imageTag := locationParts[1]
		imageNameParts := strings.Split(locationParts[0], "/")
		imageName := imageNameParts[len(imageNameParts)-1]

		p := influxdb2.NewPoint("pull",
			map[string]string{
				"region":         region,
				"namespace":      pull.Namespace,
				"owner":          pull.Owner,
				"image_location": pull.ImageLocation,
				"image_name":     imageName,
				"image_tag":      imageTag,
				"node_type":      pull.NodeType,
			},
			map[string]interface{}{
				"duration": pull.Duration.Seconds(),
			},
			t,
		)

		err := writeAPI.WritePoint(context.Background(), p)
		if err != nil {
			return errors.Wrap(err, "could not write point to influx")
		}
	}

	return nil
}
