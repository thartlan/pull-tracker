package cluster

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/containerinfra/v1/clusters"
	"github.com/pkg/errors"

	"k8s.io/klog/v2"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func CreateCluster(provider *gophercloud.ProviderClient, region, templateID string) (string, error) {
	containerClient, err := createContainerClient(provider, region)
	if err != nil {
		return "", errors.Wrap(err, "could not create client")
	}

	name := fmt.Sprintf("pull-tracker-%s", randName(8))
	klog.Info("Cluster name will be: ", name)
	clusterOpts := clusters.CreateOpts{
		ClusterTemplateID: templateID,
		Name:              name,
	}
	clusterUUID, err := clusters.Create(containerClient, clusterOpts).Extract()
	if err != nil {
		return "", errors.Wrap(err, "could not create cluster")
	}

	klog.Info("Cluster UUID is: ", clusterUUID)

	return clusterUUID, nil
}

func WaitForCluster(provider *gophercloud.ProviderClient, region string, clusterUUID string) error {
	containerClient, err := createContainerClient(provider, region)
	if err != nil {
		return errors.Wrap(err, "could not create client")
	}

	timeout := 15 * time.Minute
	startTime := time.Now()
	for time.Since(startTime) < timeout {
		c, err := clusters.Get(containerClient, clusterUUID).Extract()
		if err != nil {
			return errors.Wrap(err, "could not get cluster")
		}

		if c.Status == "CREATE_COMPLETE" {
			return nil
		}

		if c.Status != "CREATE_IN_PROGRESS" {
			return fmt.Errorf("cluster in state %s", c.Status)
		}

		time.Sleep(15 * time.Second)
	}

	return fmt.Errorf("cluster was not ready within %s", timeout)
}

func DeleteCluster(provider *gophercloud.ProviderClient, region string, clusterUUID string) error {
	containerClient, err := createContainerClient(provider, region)
	if err != nil {
		return errors.Wrap(err, "could not create client")
	}

	err = clusters.Delete(containerClient, clusterUUID).ExtractErr()
	if err != nil {
		return errors.Wrap(err, "could not delete cluster")
	}

	return nil
}

func randName(length int) string {
	chars := "abcdefghijklmnopqrstuvwxyz1234567890"

	bytes := []byte{}

	for i := 0; i < length; i++ {
		bytes = append(bytes, chars[rand.Intn(len(chars))])
	}

	return string(bytes)
}
