package cluster

import (
	"os"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"github.com/pkg/errors"
)

/*func createProviderClient() (*gophercloud.ProviderClient, error) {
	opts, err := openstack.AuthOptionsFromEnv()
	if err != nil {
		return nil, errors.Wrap(err, "could not get auth options from env")
	}

	provider, err := openstack.AuthenticatedClient(opts)
	if err != nil {
		return nil, errors.Wrap(err, "could not create authenticated client")
	}

	return provider, nil
}*/

func CreateProviderClient(cloudConfigPath string) (*gophercloud.ProviderClient, error) {
	cloudConfig, err := os.Open(cloudConfigPath)
	if err != nil {
		return nil, errors.Wrap(err, "could not open cloud config file")
	}
	defer cloudConfig.Close()

	config, err := readConfig(cloudConfig)
	if err != nil {
		return nil, errors.Wrap(err, "could not parse cloud config file")
	}

	provider, err := createProviderClientFromConfig(config)
	if err != nil {
		return nil, errors.Wrap(err, "could not create provider from config")
	}

	return provider, nil
}

func createContainerClient(provider *gophercloud.ProviderClient, region string) (*gophercloud.ServiceClient, error) {
	containerClient, err := openstack.NewContainerInfraV1(provider, gophercloud.EndpointOpts{
		Region: region,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create container infra client")
	}

	return containerClient, nil
}
