package cluster

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/containerinfra/v1/certificates"
	"github.com/gophercloud/gophercloud/openstack/containerinfra/v1/clusters"
	"github.com/pkg/errors"

	restclient "k8s.io/client-go/rest"
)

func KubeConfigForCluster(provider *gophercloud.ProviderClient, region string, clusterUUID string) (*restclient.Config, error) {
	containerClient, err := createContainerClient(provider, region)
	if err != nil {
		return nil, errors.Wrap(err, "could not create client")
	}

	c, err := clusters.Get(containerClient, clusterUUID).Extract()
	if err != nil {
		return nil, errors.Wrap(err, "could not get cluster")
	}

	certCA, err := certificates.Get(containerClient, clusterUUID).Extract()
	if err != nil {
		return nil, errors.Wrap(err, "could not get cluster certificate")
	}

	privKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, errors.Wrap(err, "could not generate private key")
	}

	var privKeyPEM bytes.Buffer
	err = pem.Encode(&privKeyPEM, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privKey)})
	if err != nil {
		return nil, errors.Wrap(err, "could not encode private key")
	}

	subj := pkix.Name{
		CommonName:   "admin",
		Organization: []string{"system:masters"},
	}

	csrReq := &x509.CertificateRequest{
		SignatureAlgorithm: x509.SHA512WithRSA,
		Subject:            subj,
	}

	csrBytes, err := x509.CreateCertificateRequest(rand.Reader, csrReq, privKey)
	if err != nil {
		return nil, errors.Wrap(err, "could not create cert request")
	}

	csrPEM := pem.EncodeToMemory(&pem.Block{
		Type:  "CERTIFICATE REQUEST",
		Bytes: csrBytes,
	})

	createOpts := certificates.CreateOpts{
		ClusterUUID: clusterUUID,
		CSR:         string(csrPEM),
	}
	cert, err := certificates.Create(containerClient, createOpts).Extract()
	if err != nil {
		return nil, errors.Wrap(err, "error getting magnum to sign certificate")
	}

	conf := &restclient.Config{
		Host: c.APIAddress,
		TLSClientConfig: restclient.TLSClientConfig{
			CertData: []byte(cert.PEM),
			KeyData:  privKeyPEM.Bytes(),
			CAData:   []byte(certCA.PEM),
		},
	}

	return conf, nil
}
