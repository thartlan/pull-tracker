package collect

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/pkg/errors"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	restclient "k8s.io/client-go/rest"
	"k8s.io/klog/v2"
)

type Pull struct {
	Namespace      string
	Owner          string
	ImageLocation  string
	NodeType       string
	EarliestPull   time.Time
	EarliestPulled time.Time
	Duration       time.Duration
}

func FindPullTimes(clusterConfig *restclient.Config) ([]*Pull, error) {
	clientset, err := kubernetes.NewForConfig(clusterConfig)
	if err != nil {
		return nil, errors.Wrap(err, "could not get k8s clientset")
	}

	pods, err := clientset.CoreV1().Pods("kube-system").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return nil, errors.Wrap(err, "could not list pods")
	}

	events := clientset.CoreV1().Events("kube-system")

	pulls := make(map[string]*Pull)

	for _, pod := range pods.Items {
		klog.Info(pod.Name)

		podEvents, err := events.List(context.TODO(), metav1.ListOptions{
			FieldSelector: fmt.Sprintf("involvedObject.name=%s", pod.Name),
		})
		if err != nil {
			return nil, errors.Wrapf(err, "could not list events for pod %s", pod.Name)
		}

		for _, event := range podEvents.Items {
			if !(event.Reason == "Pulling" || event.Reason == "Pulled") {
				continue
			}
			if strings.Contains(event.Message, "already present") {
				continue
			}
			if event.Reason == "Pulling" {
				location := event.Message[15 : len(event.Message)-1]

				nodeType := "worker"
				if strings.Contains(pod.Spec.NodeName, "master") {
					nodeType = "master"
				}

				owner, err := getPodOwnerName(clientset, &pod)
				if err != nil {
					return nil, err
				}

				timestamp := event.FirstTimestamp.UTC()
				uniq := fmt.Sprintf("%s/%s", pod.Spec.NodeName, location)
				if _, found := pulls[uniq]; !found {
					pulls[uniq] = &Pull{
						Namespace:     "kube-system",
						Owner:         owner,
						ImageLocation: location,
						NodeType:      nodeType,
						EarliestPull:  timestamp,
					}
				}
				if timestamp.Before(pulls[uniq].EarliestPull) {
					pulls[uniq].EarliestPull = timestamp
				}
			}

			if event.Reason == "Pulled" {
				timestamp := event.FirstTimestamp.UTC()
				location := event.Message[27 : len(event.Message)-1]
				uniq := fmt.Sprintf("%s/%s", pod.Spec.NodeName, location)
				if pulls[uniq].EarliestPulled.IsZero() {
					pulls[uniq].EarliestPulled = timestamp
					pulls[uniq].Duration = pulls[uniq].EarliestPulled.Sub(pulls[uniq].EarliestPull)
				}
				if timestamp.Before(pulls[uniq].EarliestPulled) {
					pulls[uniq].EarliestPulled = timestamp
					pulls[uniq].Duration = pulls[uniq].EarliestPulled.Sub(pulls[uniq].EarliestPull)
				}
			}
		}

	}

	pullList := make([]*Pull, 0)

	for _, v := range pulls {
		if v.EarliestPull.IsZero() || v.EarliestPulled.IsZero() {
			continue
		}
		klog.Infof("%s %s %s", v.Owner, v.ImageLocation, v.Duration)
		pullList = append(pullList, v)
	}

	return pullList, nil
}

func getPodOwnerName(clientset *kubernetes.Clientset, pod *corev1.Pod) (string, error) {
	if len(pod.OwnerReferences) == 0 {
		return pod.Name, nil
	}

	ownerRef := pod.OwnerReferences[0]
	if ownerRef.Kind == "ReplicaSet" {
		rs, err := clientset.AppsV1().ReplicaSets("kube-system").Get(context.TODO(), ownerRef.Name, metav1.GetOptions{})
		if err != nil {
			return "", errors.Wrapf(err, "could not get %s %s", ownerRef.Kind, ownerRef.Name)
		}

		rsOwnerRef := rs.OwnerReferences[0]
		if rsOwnerRef.Kind == "Deployment" {
			return fmt.Sprintf("deployment/%s", rsOwnerRef.Name), nil
		}
	}

	if ownerRef.Kind == "DaemonSet" {
		return fmt.Sprintf("daemonset/%s", ownerRef.Name), nil
	}

	if ownerRef.Kind == "Job" {
		return fmt.Sprintf("job/%s", ownerRef.Name), nil
	}

	return "", fmt.Errorf("owner for pod %s not found", pod.Name)
}
