FROM golang:1.15.3-alpine as builder

WORKDIR /src
COPY go.mod /src
COPY go.sum /src
RUN go mod download -x

COPY . /src
RUN CGO_ENABLED=0 go build --ldflags "-s" -o pull-tracker cmd/pull-tracker/main.go

FROM gcr.io/distroless/static

COPY --from=builder /src/pull-tracker /pull-tracker

ENTRYPOINT /pull-tracker
