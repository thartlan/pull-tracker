package main

import (
	"time"

	"gitlab.cern.ch/thartlan/pull-tracker/pkg/cluster"
	"gitlab.cern.ch/thartlan/pull-tracker/pkg/collect"
	"gitlab.cern.ch/thartlan/pull-tracker/pkg/record"

	flag "github.com/spf13/pflag"

	"k8s.io/klog/v2"
)

var region string
var clusterTemplateUUID string
var influxURL string
var cloudConfig string

func init() {
	flag.StringVar(&region, "region", "cern", "magnum region to use")
	flag.StringVar(&clusterTemplateUUID, "cluster-template", "", "cluster template to use")
	flag.StringVar(&influxURL, "influx-url", "", "influx URL to push results to")
	flag.StringVar(&cloudConfig, "cloud-config", "", "openstack cloud config file")
}

func main() {
	klog.Info("Starting")

	flag.Parse()
	if clusterTemplateUUID == "" {
		klog.Fatal("Flag cluster-template must be set")
	}
	if influxURL == "" {
		klog.Fatal("Flag influx-url must be set")
	}
	if cloudConfig == "" {
		klog.Fatal("Flag cloud-config must be set")
	}

	provider, err := cluster.CreateProviderClient(cloudConfig)
	if err != nil {
		klog.Fatalf("Error creating openstack provider: %v", err)
	}

	clusterUUID, err := cluster.CreateCluster(provider, region, clusterTemplateUUID)
	if err != nil {
		klog.Fatalf("Error creating cluster: %v", err)
	}

	cluster.WaitForCluster(provider, region, clusterUUID)

	time.Sleep(5 * time.Minute)

	kubeConfig, err := cluster.KubeConfigForCluster(provider, region, clusterUUID)
	if err != nil {
		klog.Fatalf("Could not create kube config for cluster %s: %v", clusterUUID, err)
	}

	pullTimes, err := collect.FindPullTimes(kubeConfig)
	if err != nil {
		klog.Fatalf("Error finding pull times: %v", err)
	}

	record.PushToInflux(region, influxURL, pullTimes)

	cluster.DeleteCluster(provider, region, clusterUUID)

	klog.Info("Done")
}
